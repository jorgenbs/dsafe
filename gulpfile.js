'use strict';

var gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify'),
    browserify = require('browserify'),
    gulpify = require('gulpify');

// Load plugins
var $ = require('gulp-load-plugins')();


// Styles
gulp.task('styles', function () {
    return gulp.src('app/styles/main.scss')
        .pipe($.rubySass({
            style: 'expanded',
            precision: 10,
            loadPath: ['app/bower_components']
        }))
        .pipe($.autoprefixer('last 1 version'))
        .pipe(gulp.dest('dist/styles'))
        .pipe($.size());
        //.pipe($.connect.reload());
});




// CoffeeScript
gulp.task('coffee', function () {
    return gulp.src(
            ['app/scripts/**/*.coffee', 'app/scripts/**/*.cjsx', 'app/server/*.coffee', '!app/scripts/**/*.js'],
            {base: 'app/scripts'}
        )
        .pipe(
            $.cjsx({ bare: true }).on('error', $.util.log)
            //$.coffee({ bare: true }).on('error', $.util.log)
        )
        .pipe(gulp.dest('app/scripts'))
        .pipe($.size());
});

gulp.task('compile', function () {
    var stream = gulp.src(
            ['app/scripts/**/*.coffee', 'app/scripts/**/*.cjsx', '!app/scripts/**/*.js'],
            {base: 'app/scripts'}
        )
        .pipe(
            $.cjsx({ bare: true }).on('error', $.util.log)
        )
        .pipe(gulp.dest('build/'))
        .pipe($.size())
        

    stream.on("end", function() {
        var b = browserify('./build/app.js')
            .bundle()
            .pipe(source("app.js"))
            .pipe(gulp.dest('dist/scripts'))
    });
    
    return stream;
});

//Server compile
gulp.task('compileServer', function () {
    return gulp.src('app/server/proxyServer.coffee')
        .pipe($.coffee({ bare: true }).on('error', $.util.log))
        .pipe(gulp.dest('dist/server'))
        .pipe($.size());
});

// Scripts
gulp.task('scripts', function () {
    return gulp.src('app/scripts/app.js')
        .pipe($.browserify({
            insertGlobals: true,
            transform: ['reactify']
        }))
        .pipe(gulp.dest('dist/scripts'))
        .pipe($.size());
    });
//Server compile
gulp.task('server', function () {
    return gulp.src('app/server/proxyServer.js')
        .pipe(gulp.dest('dist/server'))
        .pipe($.size());
});

gulp.task('jade', function () {
    return gulp.src('app/template/*.jade')
        .pipe($.jade({ pretty: true }))
        .pipe(gulp.dest('dist'));
        //.pipe($.connect.reload());
})

// HTML
gulp.task('html', function () {
    return gulp.src('app/*.html')
        .pipe($.useref())
        .pipe(gulp.dest('dist'))
        .pipe($.size());
        //.pipe($.connect.reload());
});

// Images
gulp.task('images', function () {
    return gulp.src('app/images/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'))
        .pipe($.size())
        //.pipe($.connect.reload());
});

// Clean
gulp.task('clean', function () {
    return gulp.src([
        'dist/styles', 
        'dist/scripts', 
        'dist/images', 
        'dist/server', 
        'app/scripts/**/*.js',
        'build/',
        'app/server/proxyServer.js'], {read: false}).pipe($.clean());
});


// Bundle
gulp.task('bundle', ['styles', 'bower'], function(){
    return gulp.src('./app/*.html')
               .pipe($.useref.assets())
               .pipe($.useref.restore())
               .pipe($.useref())
               .pipe(gulp.dest('dist'));
});

// Build
gulp.task('build', ['html', 'compile', 'compileServer', 'images', 'bundle']);
//gulp.task('build', ['scripts']);

// Default task
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

// Connect
gulp.task('connect', function() {
    
    gulp.src('dist')
        .pipe($.webserver({
            livereload: true,
            open: true,
            port: 8000
        }));    

    //start proxyserver
    var serv = require('./dist/server/proxyServer');
    serv.listen(serv.PORT);
});

// Bower helper
gulp.task('bower', function() {
    gulp.src('app/bower_components/**/*.js', {base: 'app/bower_components'})
        .pipe(gulp.dest('dist/bower_components/'));

});

gulp.task('json', function() {
    gulp.src('app/scripts/json/**/*.json', {base: 'app/scripts'})
        .pipe(gulp.dest('dist/scripts/'));
});


// Watch
gulp.task('watch', ['html', 'build', 'connect'], function () {

    // Watch .json files
    gulp.watch('app/scripts/**/*.json', ['json']);

    // Watch .html files
    gulp.watch('app/*.html', ['html']);
    
    // Watch .scss files
    gulp.watch('app/styles/**/*.scss', ['styles']);
    
    // Watch .jade files
    gulp.watch('app/template/**/*.jade', ['jade', 'html']);

    // Watch .coffeescript files
    gulp.watch(['app/scripts/**/*.coffee'], ['compile']);
    gulp.watch(['app/server/proxyServer.coffee'], ['compileServer']);

    // Watch .js files
    //gulp.watch(['app/scripts/**/*.js', '!app/scripts/server/server.js'], ['scripts']);

    //watch server src
    gulp.watch('app/scripts/server/proxyServer.js', ['server']);

    // Watch image files
    gulp.watch('app/images/**/*', ['images']);
});
