React = window.React or require 'React'

Coupon = React.createClass
  idTag: null
  
  getInitialState: ->
    return {
      idTag: "#" + @props.data.id
    }

  render: ->

    return (
      <div>
        <div className="panel-heading">
          <h4 className="panel-title">
            <a data-toggle="collapse" data-parent="#Coupons" href="#{@state.idTag}">
              {@props.data.description}
            </a>
          </h4>
        </div>
        <div className="panel-collapse collapse" id="#{@props.data.id}">
          <div className="panel-body">
            <ul className="list-group">
              value: {@props.data.value}
            </ul>
          </div>
        </div>
      </div>
    )

module.exports = Coupon