React = window.React or require 'React'

Coupon = require './Coupon'
Loading = require '../Loading'

CouponsList = React.createClass

  updateCoupons: ->
    @setState
      coupons: @props.store.findAll('coupons').map (coupon) ->
        <Coupon data={coupon} />

  getInitialState: ->
    @props.store.on 'change:coupons', @updateCoupons
    coupons: <Loading />

  render: ->
    return (
      <div className="panel-group" id="coupons">
        <h2>Coupons</h2>

        <div className="panel panel-default">
          {@state.coupons}
        </div>
      </div>
    )
  
module.exports = CouponsList