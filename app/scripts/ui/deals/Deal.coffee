React = window.React or require 'React'

Receipt = React.createClass
  idTag: null
  
  getInitialState: ->
    return {
      idTag: "#" + @props.data.id
    }

  render: ->

    return (
        <div>
          <div className="panel-heading">
            <h4 className="panel-title">
              <a data-toggle="collapse" data-parent="#receipts" href="#{@state.idTag}">
                {@props.data.title} (<i>{@props.data.subtitle}</i>)
              </a>
            </h4>
          </div>
          <div className="panel-collapse collapse" id="#{@props.data.id}">
            <div className="panel-body">
              <ul className="list-group">
                <li className="list-group-item">description: {@props.data.description}</li>
                <li className="list-group-item">original price: {@props.data.original_price}</li>
                <li className="list-group-item">discount price: {@props.data.discount_price}</li>
                <li className="list-group-item">purchased: {@props.data.purchased}</li>
              </ul>
            </div>
          </div>
        </div>
    )

module.exports = Receipt