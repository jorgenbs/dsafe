React = window.React or require 'React'

Deal = require './Deal'
Loading = require '../Loading'

DealsList = React.createClass

  updateReceipts: ->
    @setState
      deals: @props.store.findAll('deals')[0].deals.map (deal) ->
        <Deal data={deal} />

  getInitialState: ->
    @props.store.on 'change:deals', @updateReceipts
    deals: <Loading />

  render: ->
    return (
      <div className="panel-group" id="deals">
        <h2>Deals</h2>

        <div className="panel panel-default">
          {@state.deals}
        </div>  
      </div>
    )
  
module.exports = DealsList