# @cjsx React.DOM
React = window.React or require 'react'

CreateUser = require './CreateUser'
Login = require './Login'

CredentialsContainer = React.createClass
  
  getInitialState: ->
    mode: 'login'

  showCreate: ->
    @setState
      mode: 'create'

  showLogin: ->
    @setState
      mode: 'login'

  render: ->
    if not @props.loggedIn

      if @state.mode is 'login'
        return (
          <div className="jumbotron" id="login">
            <Login store={@props.store} changeMode={@showCreate} />
          </div>
        )
      else
        return (
          <div className="jumbotron" id="login">
            <CreateUser store={@props.store} changeMode={@showLogin} />
          </div>
        )

    #hide
    <div display="none"></div>

module.exports = CredentialsContainer