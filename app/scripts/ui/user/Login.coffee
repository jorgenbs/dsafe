# @cjsx React.DOM
React = window.React or require 'react'

Login = React.createClass
  
  handleSubmit: (e)->
    e.preventDefault()
    e.stopPropagation()

    @props.store.add("login",
      'email': @refs.username.getDOMNode().value
      'password': @refs.password.getDOMNode().value
    )
    
  render: () ->    
    <div className="neat-component">
      <h2>Login</h2>
      <form onSubmit={@handleSubmit}>
        <input type="text" ref="username" />
        <input type="password" ref="password" />
        <input type="submit" value="login" />
      </form>
      <a href="#" onClick={@props.changeMode} >or register</a>
    </div>

module.exports = Login