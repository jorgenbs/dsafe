# @cjsx React.DOM
React = window.React or require 'react'

Profile = React.createClass
  render: ->
    footer = <h3 className="text-muted">dSafe</h3>

    if @props.user?
      <div>
        <div className="dropdown pull-right">
          <a data-toggle="dropdown" className="btn btn-primary" href="#">{@props.user.name}</a>
          <ul className="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a href="#" onClick={@props.logout}>Logout</a></li>
            <li><a href="#">Another action</a></li>
          </ul>
        </div>
        {footer}
      </div>

    else
      #hide
      footer

module.exports = Profile