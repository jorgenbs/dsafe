# @cjsx React.DOM
React = window.React or require 'react'

CreateUser = React.createClass
  getInitialState: ->
    created: false
    user: null
  
  handleSubmit: (e) ->
    e.preventDefault()

    @props.store.update("user",
      'email': @refs.navn.getDOMNode().value
      'password': @refs.email.getDOMNode().value
      'url': '/api/v1/users'
    )

    @setState
      created: true
      user: @refs.navn.getDOMNode().value

  feedback: ->
    if @state.user != null
      return "bruker " + @state.user + " er registrert"
    ""

  render: ->    
    <div>
      <div className="neat-component">
        <h2>Create User</h2>
        <form onSubmit={@handleSubmit} >
          <input type="text" placeholder="Navn..." ref="navn" />
          <input type="text" placeholder="Email..." ref="email" />
          <input type="submit" value="Post" value="create" />
        </form>
        <a href="#" onClick={@props.changeMode} >or login</a>
      </div>
      <p className="text-success">{@feedback()}</p>
    </div>

module.exports = CreateUser