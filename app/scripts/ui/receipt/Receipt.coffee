React = window.React or require 'React'

Receipt = React.createClass
  idTag: null
  items: []
  
  getInitialState: ->
    return {
      idTag: "#" + @props.data.chain_name
      items: @props.data.items.map (item) ->
        <li className="list-group-item">{item}</li>
    }

  render: ->

    return (
        <div>
          <div className="panel-heading">
            <h4 className="panel-title">
              <a data-toggle="collapse" data-parent="#receipts" href="#{@state.idTag}">
                {@props.data.chain_name}, kr. {@props.data.gross_amount} {@props.data.currency}
              </a>
            </h4>
          </div>
          <div className="panel-collapse collapse" id="#{@props.data.chain_name}">
            <div className="panel-body">
              <ul className="list-group">
                {@state.items}
              </ul>
            </div>
          </div>
        </div>
    )

module.exports = Receipt