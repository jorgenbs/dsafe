React = window.React or require 'React'

Receipt = require './Receipt'
Loading = require '../Loading'

ReceiptList = React.createClass

  updateReceipts: ->
    @setState
      receipts: @props.store.findAll('receipts')[0].receipts.map (r) ->
        <Receipt data={r} />

  getInitialState: ->
    @props.store.on 'change:receipts', @updateReceipts
    receipts: <Loading />

  render: ->
    return (
      <div className="panel-group" id="receipts">
        <h2>Receipts</h2>

        <div className="panel panel-default">
          {@state.receipts}
        </div>  
      </div>
    )
  
module.exports = ReceiptList