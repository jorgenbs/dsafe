React = window.React or require 'react'
Amygdala = require 'Amygdala'

Profile = require './ui/user/Profile'
CredentialsContainer = require './ui/user/CredentialsContainer'

ReceiptList = require './ui/receipt/ReceiptList'
DealsList = require './ui/deals/DealsList'
CouponList = require './ui/coupons/CouponList'

Store = new Amygdala
    'config':
      'apiUrl': 'http://176.58.124.19:2000'
      'localStorage': true
      'idAttribute': 'url'
    'schema':
      'login':
        'url': '/api/v1/users/login'
        'idAttribute': 'id'
      'receipts':
        'url': '/api/v1/users'
      'deals':
        'url': '/api/v1/users'
      'coupons':
        'url': '/api/v1/users'

App = React.createClass

  store: Store
  receipts: []
  deals: []
  coupons: []

  logout: () ->
    #bit weird
    window.localStorage.removeItem 'amy-login'
    @setState
      user: null
  
  login: (usr) ->
    if usr?
      @setState
        user: usr
      @getUserInfo()

  getUserInfo: ->
    @store.get('receipts', @receipts, {url: '/api/v1/users/' + @state.user.id + '/receipts'})
    @store.get('deals', @deals, {url: '/api/v1//users/' + @state.user.id + '/deals'})
    @store.get('coupons', @deals, {url: '/api/v1/users/' + @state.user.id + '/coupons'})

  authenticate: () ->
    [session] = store.findAll('login')
    @login(session)

  componentDidMount: ->
    @store.on("change:login", @authenticate)

  getInitialState: () ->
    user: null

  render: () ->
    userLists = if @state.user?
      <div id="content" className="row marketing" id="output">
        <ReceiptList store={@store} />
        <DealsList store={@store} />
        <CouponList store={@store} />
      </div>
    display = if @state.user? then "block" else "none" 


    <div className="container">
      <div className="header">
        <Profile logout={@logout} user={@state.user} />
      </div>

      <CredentialsContainer loggedIn={@state.user?} store={@store} />

      <div id="content" className="row marketing" id="output" display="{display}">
        {userLists}
      </div>

      <div className="footer">
      </div>
    </div>

React.renderComponent(
  <App store={Store} />,
  document.getElementById('app')
)

window.store = Store