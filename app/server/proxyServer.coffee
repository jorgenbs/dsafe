"use strict"
#hmm
makeProxyRequestOptions = (original_req) ->
  hostname: "private-2906e-dsafe.apiary-mock.com"
  port: 80
  path: original_req.url
  method: "GET"
onRequest = (client_req, client_res) ->
  options = makeProxyRequestOptions(client_req)
  client_res.setHeader "Content-Type", "application/json"
  client_res.setHeader "Access-Control-Allow-Origin", "*"
  client_res.setHeader "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS,TRACE,HEAD"
  client_res.setHeader "Access-Control-Allow-Headers", "Content-Type,Authorization,Accept,X-Requested-With"
  proxy = http.request(options, (res) ->
    res.pipe client_res,
      end: true

    return
  )
  client_req.pipe proxy,
    end: true

  return

http = require("http")
PORT = 2000

#http.createServer(onRequest).listen(PORT);
module.exports = http.createServer(onRequest).listen(PORT)
module.exports.PORT = PORT